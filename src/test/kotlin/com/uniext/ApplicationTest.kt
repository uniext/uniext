package com.uniext

import com.uniext.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlin.test.*
import kotlin.test.assertContains

// Essa é o arquivo onde iremos escrever os testes:

class ApplicationTest {
    // @Test
    // fun testRoot() = testApplication {
    //     application {
    //         configureRouting()
    //     }
    //     client.get("/").apply {
    //         assertEquals(HttpStatusCode.OK, status)
    //         assertEquals("Hello World!", bodyAsText())
    //     }
    // }

    @Test
    fun testNewEndpoint() = testApplication {
        application {
            module()
        }

        val response = client.get("/teste1")

        assertEquals(HttpStatusCode.OK, response.status)
        assertEquals("html", response.contentType()?.contentSubtype)
        assertContains(response.bodyAsText(), "Hello From Ktor")
    }
}
