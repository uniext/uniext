# ✨ UniExt - Gerenciamento de Grupos de Extensão Universitário

Repositório aposentado, para ver os códigos, acesse esses outros 2 Repositórios por favor:
- [UniExt - Frontend](https://gitlab.com/uniext/uniext-frontend);
- [UniExt - API Backend](https://gitlab.com/uniext/uniext-backend-api);